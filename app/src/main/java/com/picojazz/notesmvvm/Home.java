package com.picojazz.notesmvvm;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.picojazz.notesmvvm.adapter.NoteAdapter;
import com.picojazz.notesmvvm.entities.Note;
import com.picojazz.notesmvvm.entities.User;
import com.picojazz.notesmvvm.viewModel.NoteViewModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator;

public class Home extends AppCompatActivity {

    private NoteViewModel noteViewModel;
    private User user;
    public static final int ADD_NOTE_REQUEST = 1;
    public static final int EDIT_NOTE_REQUEST = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        final RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        FloatingActionButton floatingActionButton = findViewById(R.id.floatbutton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, addeditNote.class);
                startActivityForResult(intent, ADD_NOTE_REQUEST);
            }
        });


        final NoteAdapter adapter = new NoteAdapter();
        recyclerView.setAdapter(adapter);

        user = (User) getIntent().getSerializableExtra("user");


        ViewModelProvider.Factory factory = new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new NoteViewModel(getApplication(), user);
            }
        };

        noteViewModel = new ViewModelProvider(this, factory).get(NoteViewModel.class);
        noteViewModel.getAllNotes().observe(this, new Observer<List<Note>>() {
            @Override
            public void onChanged(List<Note> notes) {

                adapter.setNotes(notes);

                /*for (Note note : notes) {
                    System.out.println(note.getTitle()+" "+note.getContent()+" "+note.getUserLogin());
                }*/

            }
        });


        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                 final Note deletedNote = adapter.getNoteAt(viewHolder.getAdapterPosition());
                noteViewModel.delete(deletedNote);
                Snackbar.make(recyclerView,"Note supprimée",Snackbar.LENGTH_LONG).setAction("annuler", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noteViewModel.insert(deletedNote);

                    }
                }).show();


            }

            @Override
            public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                new RecyclerViewSwipeDecorator.Builder(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                        .addSwipeLeftBackgroundColor(ContextCompat.getColor(Home.this, R.color.red))
                        .addActionIcon(R.drawable.ic_delete_black_24dp)
                        .addSwipeLeftLabel("supprimer")
                        .setSwipeLeftLabelColor(ContextCompat.getColor(Home.this, R.color.white))
                        .create()
                        .decorate();


                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }



        }).attachToRecyclerView(recyclerView);


        adapter.setOnItemClickListener(new NoteAdapter.onItemClickListener() {
            @Override
            public void onItemClick(Note note) {
                Intent intent = new Intent(Home.this, addeditNote.class);
                intent.putExtra("title",note.getTitle());
                intent.putExtra("content",note.getContent());
                intent.putExtra("id",note.getId());
                intent.putExtra("date",note.getCreatedAt());
                startActivityForResult(intent,EDIT_NOTE_REQUEST);

            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_NOTE_REQUEST && resultCode == RESULT_OK) {
            Note note = new Note(data.getStringExtra("title"), data.getStringExtra("content"));
            note.setUserLogin(user.getLogin());
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");

            note.setCreatedAt("Crée : " + formatter.format(date));
            noteViewModel.insert(note);
            Toast.makeText(this, "Note enregistrée", Toast.LENGTH_SHORT).show();

        }else if(requestCode == EDIT_NOTE_REQUEST && resultCode == RESULT_OK){
            int id = data.getIntExtra("id",-1);
            if(id == -1){
                Toast.makeText(this,"Note pas modifiée",Toast.LENGTH_SHORT).show();
                return;
            }
            Note note = new Note(data.getStringExtra("title"), data.getStringExtra("content"));
            note.setId(id);
            note.setCreatedAt(data.getStringExtra("date"));
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");

            note.setModifiedAt("modifiée : " + formatter.format(date));
            note.setUserLogin(user.getLogin());
            noteViewModel.update(note);
            Toast.makeText(this, "Note modifiée", Toast.LENGTH_SHORT).show();


        }else{
            Toast.makeText(this, "Note pas enregistrée ", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.homemenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.loggut:
                Intent intent = new Intent(Home.this,MainActivity.class);
                startActivity(intent);
                finish();
            case R.id.suppAllnotes:
                noteViewModel.deleteAllNotes();
                Toast.makeText(this, "Toutes les notes sont supprimées", Toast.LENGTH_SHORT).show();
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
