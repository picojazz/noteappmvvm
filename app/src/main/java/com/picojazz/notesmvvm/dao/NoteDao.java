package com.picojazz.notesmvvm.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.picojazz.notesmvvm.entities.Note;

import java.util.List;
@Dao
public interface NoteDao {
    @Insert
    void insert(Note note);
    @Update
    void update(Note note);
    @Delete
    void delete(Note note);
    @Query("Delete from note_table where userLogin= :login")
    void deteleAllNotes(String login);
    @Query("select * from note_table where userLogin=:login order by id desc ")
     public LiveData<List<Note>> getNotes(String login);



}
