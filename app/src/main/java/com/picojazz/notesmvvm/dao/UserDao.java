package com.picojazz.notesmvvm.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.picojazz.notesmvvm.entities.User;


import java.util.List;

@Dao
public interface UserDao {
    @Insert
    void insert(User user);
    @Update
    void update(User user);
    @Delete
    void delete(User user);
    @Query("Select * from user_table where login=:login and password = :password")
    public User connect(String login,String password);
    @Query("Select * from user_table where login=:login")
    public User getUser(String login);
}
