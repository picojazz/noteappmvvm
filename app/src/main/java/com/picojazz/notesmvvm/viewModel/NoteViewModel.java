package com.picojazz.notesmvvm.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.picojazz.notesmvvm.entities.Note;
import com.picojazz.notesmvvm.entities.User;
import com.picojazz.notesmvvm.repository.NoteRepository;

import java.util.List;

public class NoteViewModel extends AndroidViewModel {
    private NoteRepository repo;
    private LiveData<List<Note>> allNotes;
    //private User user;

    public NoteViewModel(@NonNull Application application,User user) {
        super(application);
        repo = new NoteRepository(application,user);
        allNotes = repo.getAllNotes();
    }
    public void insert(Note note){
        repo.insert(note);
    }
    public void update(Note note){
        repo.update(note);
    }
    public void delete(Note note){
        repo.delete(note);
    }
    public void deleteAllNotes(){
        repo.deleteAllNotes();
    }

    public LiveData<List<Note>> getAllNotes() {
        return allNotes;
    }
}
