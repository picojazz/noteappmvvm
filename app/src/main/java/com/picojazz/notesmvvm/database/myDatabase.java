package com.picojazz.notesmvvm.database;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.picojazz.notesmvvm.dao.NoteDao;

import com.picojazz.notesmvvm.dao.UserDao;
import com.picojazz.notesmvvm.entities.Note;
import com.picojazz.notesmvvm.entities.User;

@Database(entities = {User.class, Note.class},version = 2)
public abstract class myDatabase extends RoomDatabase {

    private static myDatabase instance;

    public abstract NoteDao NoteDao();

    public abstract UserDao UserDao();

    public static synchronized myDatabase getInstance(Context context){

        if(instance == null){
            instance = Room.databaseBuilder(context.getApplicationContext(),myDatabase.class,"myDatabase").fallbackToDestructiveMigration().build();
        }
        return instance;
    }

    public static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };
    public static class PopulateDbAsyncTask extends AsyncTask<Void,Void,Void>{
        private NoteDao noteDao;
        private UserDao userDao;
        public PopulateDbAsyncTask(myDatabase db){
            this.noteDao = db.NoteDao();
            this.userDao = db.UserDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            /*Note note = new Note("note1","fdsfgdsfsd");
            note.setUserLogin("pico");
            noteDao.insert(note);*/
            return null;
        }
    }
}
