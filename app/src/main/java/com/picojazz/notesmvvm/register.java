package com.picojazz.notesmvvm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.picojazz.notesmvvm.dao.UserDao;
import com.picojazz.notesmvvm.database.myDatabase;
import com.picojazz.notesmvvm.entities.User;

public class register extends AppCompatActivity {
    Button btnConnectR ,btnSignUp;
    EditText txtUsername,txtPassword,txtNom;
    String username,password,nom;
    UserDao userDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_register);

        getSupportActionBar().hide();


        btnConnectR = findViewById(R.id.btnConnectR);
        btnSignUp = findViewById(R.id.btnSignUpR);
        txtUsername = findViewById(R.id.txtUsernameR);
        txtPassword = findViewById(R.id.txtPasswordR);
        txtNom = findViewById(R.id.nom);

        userDao = Room.databaseBuilder(this, myDatabase.class, "myDatabase.db")
                .allowMainThreadQueries()
                .build()
                .UserDao();


        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                username = txtUsername.getText().toString();
                password = txtPassword.getText().toString();
                nom = txtNom.getText().toString();
                if(username.isEmpty() || password.isEmpty()){
                    Toast.makeText(register.this, "veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
                }else {
                    User user = new User(nom, username, password);
                    User userConnect = userDao.getUser(username);
                    if (userConnect != null) {
                        Toast.makeText(register.this, "cet identifiant est deja pris", Toast.LENGTH_SHORT).show();
                    } else {
                        userDao.insert(user);
                        Toast.makeText(register.this, "compte enregistrée", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        btnConnectR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(register.this,MainActivity.class);
                //startActivity(intent);
                finish();
            }
        });

    }
}
