package com.picojazz.notesmvvm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.picojazz.notesmvvm.dao.UserDao;
import com.picojazz.notesmvvm.database.myDatabase;
import com.picojazz.notesmvvm.entities.User;

public class MainActivity extends AppCompatActivity {

    Button signUp,login;
    EditText txtUsername,txtPassword;
    String username,password;
    ProgressDialog dialog;
    UserDao userDao;
    //myDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().hide();


        txtUsername = findViewById(R.id.txtUsername);
        txtPassword = findViewById(R.id.txtPassword);
        dialog = new ProgressDialog(this);
        dialog.setMessage("connexion en cours");

        //database = myDatabase.getInstance(getApplicationContext());
        userDao = Room.databaseBuilder(this, myDatabase.class, "myDatabase.db")
                .fallbackToDestructiveMigration()
                .allowMainThreadQueries()
                .build()
                .UserDao();

        login = findViewById(R.id.btnConnect);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = txtUsername.getText().toString();
                password = txtPassword.getText().toString();
                if(username.isEmpty() || password.isEmpty()){
                    Toast.makeText(MainActivity.this, "veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
                }else {
                    User user = userDao.connect(username, password);
                    if (user != null) {
                        Toast.makeText(MainActivity.this, "Bienvenue "+user.getName(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity.this,Home.class);
                        intent.putExtra("user",user);
                        startActivity(intent);
                        finish();

                    } else {
                        Toast.makeText(MainActivity.this, "Nom d'utilisateur/Mot de passe incorrect", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        signUp = findViewById(R.id.btnSignUp);
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,register.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        txtPassword.setText("");

    }
}
