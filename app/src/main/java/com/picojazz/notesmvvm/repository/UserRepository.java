package com.picojazz.notesmvvm.repository;

import android.app.Application;
import android.os.AsyncTask;

import com.picojazz.notesmvvm.dao.UserDao;
import com.picojazz.notesmvvm.database.myDatabase;
import com.picojazz.notesmvvm.entities.Note;
import com.picojazz.notesmvvm.entities.User;

public class UserRepository {
    private UserDao userDao;
    private User user;

    public UserRepository(Application application,User user){
        myDatabase database = myDatabase.getInstance(application);
        userDao = database.UserDao();
        this.user = user;
    }

    public void insert(User user){
        new InsertUserAsyncTask(userDao).execute(user);
    }
    public void getUser(){
        //return new getUserAsyncTask(userDao,user).execute();
    }



    private  static class InsertUserAsyncTask  extends AsyncTask<User,Void,Void> {
        private UserDao userDao;

        public InsertUserAsyncTask(UserDao userDao){
            this.userDao = userDao;
        }

        @Override
        protected Void doInBackground(User... users) {
            userDao.insert(users[0]);
            return null;
        }
    }
    private  static class getUserAsyncTask  extends AsyncTask<Void,Void,User> {
        private UserDao userDao;
        private User user;

        public getUserAsyncTask(UserDao userDao,User user){
            this.userDao = userDao;
            this.user = user;
        }

        @Override
        protected User doInBackground(Void... voids) {
            User muser = userDao.getUser(user.getLogin());
            return user;
        }

        @Override
        protected void onPostExecute(User user) {
            super.onPostExecute(user);

        }
    }
}
