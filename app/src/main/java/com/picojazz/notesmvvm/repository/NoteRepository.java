package com.picojazz.notesmvvm.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.picojazz.notesmvvm.dao.NoteDao;
import com.picojazz.notesmvvm.database.myDatabase;
import com.picojazz.notesmvvm.entities.Note;
import com.picojazz.notesmvvm.entities.User;

import java.util.List;

public class NoteRepository {

    private NoteDao noteDao;
    private LiveData<List<Note>> allNotes;
    private User user;

    public NoteRepository(Application application, User user){
        myDatabase database = myDatabase.getInstance(application);
        noteDao = database.NoteDao();
        allNotes = noteDao.getNotes(user.getLogin());
        this.user = user;
    }
    public void insert(Note note){
        new InsertNoteAsyncTask(noteDao).execute(note);
    }

    public void update(Note note){
        new UpdateNoteAsyncTask(noteDao).execute(note);
    }

    public void delete(Note note){
        new DeleteNoteAsyncTask(noteDao).execute(note);
    }

    public void deleteAllNotes(){
        new DeleteAllNoteAsyncTask(noteDao,user).execute();
    }

    public LiveData<List<Note>> getAllNotes(){
        return allNotes;
    }


    private  static class InsertNoteAsyncTask  extends AsyncTask<Note,Void,Void>{
        private NoteDao noteDao;

        public InsertNoteAsyncTask(NoteDao noteDao){
            this.noteDao = noteDao;
        }
        @Override
        protected Void doInBackground(Note... notes) {
            noteDao.insert(notes[0]);
            return null;
        }
    }
    private  static class UpdateNoteAsyncTask  extends AsyncTask<Note,Void,Void>{
        private NoteDao noteDao;

        public UpdateNoteAsyncTask(NoteDao noteDao){
            this.noteDao = noteDao;
        }
        @Override
        protected Void doInBackground(Note... notes) {
            noteDao.update(notes[0]);
            return null;
        }
    }
    private  static class DeleteNoteAsyncTask  extends AsyncTask<Note,Void,Void>{
        private NoteDao noteDao;

        public DeleteNoteAsyncTask(NoteDao noteDao){
            this.noteDao = noteDao;
        }
        @Override
        protected Void doInBackground(Note... notes) {
            noteDao.delete(notes[0]);
            return null;
        }
    }
    private  static class DeleteAllNoteAsyncTask  extends AsyncTask<Void,Void,Void>{
        private NoteDao noteDao;
        private User user;

        public DeleteAllNoteAsyncTask(NoteDao noteDao,User user){
            this.noteDao = noteDao;
            this.user = user;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            noteDao.deteleAllNotes(user.getLogin());
            return null;
        }
    }



}
