package com.picojazz.notesmvvm;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class addeditNote extends AppCompatActivity {
    EditText editTexttitle, editTextcontent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        editTexttitle = findViewById(R.id.titreAdd);
        editTextcontent = findViewById(R.id.contentAdd);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        Intent intent = getIntent();
        if(intent.hasExtra("id")) {
            setTitle("Editer une Note ");
            editTexttitle.setText(intent.getStringExtra("title"));
            editTextcontent.setText(intent.getStringExtra("content"));
        }else{
            setTitle("Ajouter une NOte ");
        }
    }

    public void saveNote() {
        String title = editTexttitle.getText().toString();
        String content = editTextcontent.getText().toString();
        if (title.trim().isEmpty() || content.trim().isEmpty()) {
            Toast.makeText(this, "veuillez remplir les champs", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent data = new Intent();
        data.putExtra("title", title);
        data.putExtra("content", content);

        int id = getIntent().getIntExtra("id",-1);
        if( id != -1){
            data.putExtra("id", id);
            data.putExtra("date",getIntent().getStringExtra("date"));
        }

        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.addnotemenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.saveAdd:
                saveNote();

                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }


}
