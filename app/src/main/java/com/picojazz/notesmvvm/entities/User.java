package com.picojazz.notesmvvm.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.List;

@Entity(tableName="user_table")
public class User implements Serializable {

    private String name;
    @PrimaryKey @NonNull
    private String login;
    private  String password;
    @Ignore
    private List<Note> notes;

    public User() {
    }
    @Ignore
    public User(String name, String login, String password) {
        this.name = name;
        this.login = login;
        this.password = password;

    }

    public String getName() {
        return name;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }
}
